define(['app'],
  function (App) { 'use strict';

  return Marionette.LayoutView.extend({
    el      : 'body',
    regions : {
      headerRegion    : '.layout-header',
      contentRegion   : '.layout-content',
      signinRegion    : '.layout-signin',
    },
    initialize: function () {
      var context = this;
      require(['views/partials/header/DefaultView'], function (DefaultView) {
        context.headerRegion.$el.css({'background-color': '#000'});
        context.headerRegion.show(new DefaultView());
      });
      UIkit.domObserve(this.$el);
    },
  });
});