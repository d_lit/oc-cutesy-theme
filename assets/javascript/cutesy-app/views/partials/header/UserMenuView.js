define(['app'],
    function (App) { 'use strict';

    return App.Views.OctoberPartial.extend({
        tagName     : 'ul',
        className   : 'uk-navbar-nav',
        partialFile : 'header/user-menu',
        initialize: function(options) {
            this.constructor.__super__.initialize.apply(this, arguments);
        },
    });
});