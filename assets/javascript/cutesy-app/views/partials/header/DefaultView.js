define(['app'],
    function (App) { 'use strict';

    return App.Views.OctoberPartial.extend({
        className   : 'uk-container uk-container-center',
        partialFile : 'header/default',
        ui          : {
            userMenu: '.user-menu',
        },
        initialize: function (options) {
            this.constructor.__super__.initialize.apply(this, arguments);
        },
    });
});