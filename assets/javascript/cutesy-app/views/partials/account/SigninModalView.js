define(['app', 'behaviors/FormBehavior'],
  function (App, FormBehavior) { 'use strict';

  return App.Views.OctoberPartial.extend({
    className   : 'uk-modal-dialog',
    partialFile : 'account/modal_signin',
    behaviors: {
      FormBehavior: {
        behaviorClass   : FormBehavior,
        channelName     : 'accountChannel',
        ajaxHandler     : 'onSignin',
      },
    },
    ui: {
      'signinForm': '#accountSigninForm',
    },
    initialize: function(options) {
      this.constructor.__super__.initialize.apply(this, arguments);

      this.vent.on('partial:ready', function (OctoberPartialObj) {
        var rules = {
            'email': {
              required: true,
              email: true
            },
            'password': {
              required: true,
              minlength : 5
            },
          },
          messages = {

          };

        $(OctoberPartialObj.ui.signinForm).validate({
          rules: rules,
          messages: messages,
          errorClass: 'uk-form-danger',
          submitHandler: function(form) {
            OctoberPartialObj.triggerMethod('FormSubmit', form);
          }
        });
      });
    },
  });
});