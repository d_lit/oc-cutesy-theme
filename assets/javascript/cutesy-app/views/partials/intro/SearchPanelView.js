define(['app', 'behaviors/FormBehavior'],
  function (App, FormBehavior) { 'use strict';

    var PanelView = App.Views.OctoberPartial.extend({
      className   : 'search-panel-container uk-contrast',
      partialFile : 'community/search_panel',
      behaviors   : {
        FormBehavior: {
          behaviorClass   : FormBehavior,
          channelName     : 'introChannel',
          ajaxHandler     : 'onSearch',
        },
      },
      ui: {
        'searchPanelForm': '#searchPanelForm',
      },
      initialize: function(options) {
        this.constructor.__super__.initialize.apply(this, arguments);

        this.vent.on('partial:ready', function (OctoberPartialObj) {
          var form = $(OctoberPartialObj.ui.searchPanelForm);

          form.validate({
            submitHandler: function(form) {
              OctoberPartialObj.onSearch(form);
              return false;
            }
          });
        });
      },
      onSearch: function(form) {
        var query = $(form).serialize();
        App.Community.API.searchProfiles(query);
      },
    });

    return PanelView;
});