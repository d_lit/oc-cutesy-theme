define(['app'],
  function (App) { 'use strict';

    var MapView = App.Views.OctoberPartial.extend({
      className   : 'search-map-container',
      partialFile : 'community/search_map',
      initialize: function(options) {
        this.constructor.__super__.initialize.apply(this, arguments);

        this.vent.on('partial:ready', function (OctoberPartialObj) {

        });
      }
    });

    return MapView;
});