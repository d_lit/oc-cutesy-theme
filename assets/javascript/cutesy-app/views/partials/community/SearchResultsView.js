define(['app'],
  function (App) { 'use strict';

    var ResultsView = App.Views.OctoberPartial.extend({
      className   : 'uk-container uk-container-center',
      partialFile : 'community/search_results',

      ui          : {
        title     : '.search-results-title',
        list      : '.search-results-list',
        scrollspy : '.search-results-scrollspy',
        item      : '.results-list-item',
      },

      events      : {
        'click @ui.item': 'showProfile',
      },

      initialize: function (options) {
        this.constructor.__super__.initialize.apply(this, arguments);

        this.listenTo(App.communityChannel.vent, 'search:start', this.onSearchStart);
        this.listenTo(App.communityChannel.vent, 'search:request:success', this.onRequestSuccess);
      },

      onSearchStart: function () {
        $(this.ui.title).empty();
        $(this.ui.list).empty();
      },

      onRequestSuccess: function (response) {
        if (response.title) {
          this.applyTitle(response.title);
        }
        this.applyResults(response.list);
      },

      applyTitle: function (title) {
        $(this.ui.title).hide().html(title).fadeIn();
        App.routerChannel.commands.execute('setTitle', $(this.ui.title).text());
      },

      applyResults: function (list) {
        $(this.ui.list).css({'opacity': 0}).append(list);

        UIkit.grid($(list).find('.uk-grid'),  {'gutter': 20});
        // hide UIkit glitch
        setTimeout(_.bind(this.showResults, this), 400);
      },

      showResults: function () {
        $(this.ui.list).animate({'opacity': 1}, 400);
        App.communityChannel.vent.trigger('search:done');
      },

      onDestroy: function () {
        this.stopListening(App.communityChannel.vent, 'search:start');
        this.stopListening(App.communityChannel.vent, 'search:request:success');
      },

    });

  return ResultsView;
});