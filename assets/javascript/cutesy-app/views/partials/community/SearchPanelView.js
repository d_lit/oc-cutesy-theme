define(['app', 'behaviors/FormBehavior'],
  function (App, FormBehavior) { 'use strict';

    var PanelView = App.Views.OctoberPartial.extend({
      className   : 'search-panel-container uk-contrast',
      partialFile : 'community/search_panel',
      behaviors   : {
        FormBehavior: {
          behaviorClass   : FormBehavior,
          channelName     : 'communityChannel',
          ajaxHandler     : 'onSearch',
        },
      },
      ui: {
        'searchPanelForm': '#searchPanelForm',
      },
      initialize: function(options) {
        this.constructor.__super__.initialize.apply(this, arguments);

        this.vent.on('partial:ready', function (OctoberPartialObj) {

          var pageContainer = $('.cutesy-community.search');
          var parentEl = OctoberPartialObj._parent.$el;

          pageContainer.scroll(function () {
            if (pageContainer.scrollTop() >= 430) {
              if (!parentEl.hasClass('fixed')) {
                parentEl.hide().addClass('fixed').fadeIn();
              }
            } else {
              parentEl.removeClass('fixed');
            }
          });

          var form = $(OctoberPartialObj.ui.searchPanelForm);

          form.validate({
            submitHandler: function(form) {
              OctoberPartialObj.onSearch(form);
              return false;
            }
          });
          /* initial search */
          if (OctoberPartialObj.options.autostart === true) {
            OctoberPartialObj.onSearch(form);
          }
        });
      },
      onSearch: function(form) {
        var pageContainer = $('.cutesy-community.search');
        var context = this;

        if (pageContainer.scrollTop() > 0) {
          pageContainer.animate({scrollTop: 0}, 800, 'easeInOutQuad', function () {
            context.triggerMethod('FormSubmit', form);
            App.communityChannel.vent.trigger('search:start', form);
          });
        } else {
          context.triggerMethod('FormSubmit', form);
          App.communityChannel.vent.trigger('search:start', form);
        }

      },
    });

    return PanelView;
});