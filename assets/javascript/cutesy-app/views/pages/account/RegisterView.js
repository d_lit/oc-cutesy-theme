define(['app', 'behaviors/FormBehavior'],
  function (App, FormBehavior) { 'use strict';

  return App.Views.OctoberPage.extend({
    className   : 'cutesy-account register',
    pageFile    : 'account/register',
    behaviors: {
      FormBehavior: {
        behaviorClass   : FormBehavior,
        channelName     : 'accountChannel',
        ajaxHandler     : 'onRegister',
      },
    },
    ui: {
      'registerBeautyForm'  : '#registerBeautyForm',
      'registerProForm'     : '#registerProForm',
      'registerTabs'        : '#registerTabs',
    },
    events: {

    },
    initialize: function(options) {
      this.constructor.__super__.initialize.apply(this, arguments);

      this.vent.on('page:ready', function (OctoberPageObj) {
        var rules = {
            'name': {
              required: true,
            },
            'surname': {
              required: true,
            },
            'email': {
              required: true,
              email: true
            },
          },
          messages = {

          };

        $(OctoberPageObj.ui.registerBeautyForm).validate({
          rules: _.extend(rules, {
            'password_beauty': {
              required: true,
              minlength : 5
            },
            'password_beauty_confirmation': {
              required: true,
              minlength : 5,
              equalTo : '[name="password_beauty"]'
            }
          }),
          messages: messages,
          errorClass: 'uk-form-danger',
          errorPlacement: function(error, element) {
            error.prependTo(element.parent("div"));
          },
          submitHandler: function(form) {
            OctoberPageObj.triggerMethod('FormSubmit', form);
          }
        });

        $(OctoberPageObj.ui.registerProForm).validate({
          rules: _.extend(rules, {
            'password_pro': {
              required: true,
              minlength : 5
            },
            'password_pro_confirmation': {
              required: true,
              minlength : 5,
              equalTo : '[name="password_pro"]'
            }
          }),
          messages: messages,
          errorClass: 'uk-form-danger',
          errorPlacement: function(error, element) {
            error.prependTo(element.parent("div"));
          },
          submitHandler: function(form) {
            OctoberPageObj.triggerMethod('FormSubmit', form);
          }
        });
      });
    },
  });
});