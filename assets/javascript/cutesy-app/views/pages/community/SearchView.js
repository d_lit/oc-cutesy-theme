define(['app', 'behaviors/ProgressBehavior'],
  function (App, ProgressBehavior) { 'use strict';

    var SearchView = App.Views.OctoberPage.extend({
      className   : 'cutesy-community search',
      pageFile    : 'community/search',
      behaviors   : {
        ProgressBehavior: {
          behaviorClass: ProgressBehavior,
          message: 'Отбираем самых лучших…'
        },
      },
      initialize: function(options) {
        this.constructor.__super__.initialize.apply(this, arguments);

        this.listenTo(App.communityChannel.vent, 'search:start', this.onSearchStart);
        this.listenTo(App.communityChannel.vent, 'search:done', this.onSearchDone);

        this.vent.on('page:ready', function (OctoberPageObj) {

          OctoberPageObj.addRegions({
            mapRegion    : '.community-search-map',
            panelRegion  : '.community-search-panel',
            resultsRegion: '.community-search-results',
          });

          require([
              'views/partials/community/SearchMapView',
              'views/partials/community/SearchPanelView',
              'views/partials/community/SearchResultsView'
            ], function (MapView, PanelView, ResultsView) {

              var mapView = new MapView();

              var panelView = new PanelView({
                autostart: true,
                parameters: {
                  terms: options.terms
                }
              });

              var resultsView = new ResultsView();

              OctoberPageObj.mapRegion.show(mapView);
              OctoberPageObj.panelRegion.show(panelView);
              OctoberPageObj.resultsRegion.show(resultsView);
          });
        });
      },
      onSearchStart: function (form) {
        //this.triggerMethod('ShowProgress');
      },
      onSearchDone: function (form) {
        this.triggerMethod('HideProgress');
      },
      onDestroy: function () {
        this.stopListening(App.communityChannel.vent, 'search:start');
        this.stopListening(App.communityChannel.vent, 'search:done');
      },
    });

    return SearchView;
});