define(['app', 'behaviors/FormBehavior'],
  function (App, FormBehavior) { 'use strict';

  return App.Views.OctoberPage.extend({
    className   : 'cutesy-profile complete',
    pageFile    : 'profile/complete',
    behaviors: {
      FormBehavior: {
        behaviorClass   : FormBehavior,
        channelName     : 'profileChannel',
        ajaxHandler     : 'onComplete',
      },
    },
    ui: {
      'completeBeautyForm'  : '#completeBeautyForm',
      'completeProForm'     : '#completeProForm',
      'portraitUploader'    : '[data-unique-id="portraitUploader"]',
      'photosUploader'      : '[data-unique-id="photosUploader"]',
    },
    events: {

    },
    initialize: function(options) {
      this.constructor.__super__.initialize.apply(this, arguments);

      this.vent.on('page:ready', function (OctoberPageObj) {
        var rules = {
            'name' : {
              required: true,
            },
            'surname' : {
              required: true,
            },
            'phone' : {

            },
            'email' : {
              required: true,
              email: true
            },
          },
          messages = {
            'name': 'Пожалуйста, укажите ваше имя',
            'surname': 'Пожалуйста, укажите вашу фамилию',
            'phone': '',
            'email': {
              required: 'Очень важное поле',
              email: 'Неверный формат адреса'
            },
          };

        $(OctoberPageObj.ui.completeBeautyForm).validate({
          rules: rules,
          messages: messages,
          errorClass: 'uk-form-danger',
          errorPlacement: function(error, element) {
              error.prependTo(element.parent("div"));
          },
          submitHandler: function(form) {
            OctoberPageObj.triggerMethod('FormSubmit', form);
          }
        });

        $(OctoberPageObj.ui.completeProForm).validate({
          rules: rules,
          messages: messages,
          errorClass: 'uk-form-danger',
          errorPlacement: function(error, element) {
              error.prependTo(element.parent("div"));
          },
          submitHandler: function(form) {
            OctoberPageObj.triggerMethod('FormSubmit', form);
          }
        });

        $(OctoberPageObj.ui.portraitUploader).fileUploader();
        $(OctoberPageObj.ui.photosUploader).fileUploader();
      });
    },
  });
});