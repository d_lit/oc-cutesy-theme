define(['app', 'behaviors/ProgressBehavior'],
  function (App, ProgressBehavior) { 'use strict';

  return App.Views.OctoberPage.extend({
    className   : 'cutesy-profile show',
    pageFile    : 'profile/show',
    behaviors   : {
      ProgressBehavior: {
        behaviorClass: ProgressBehavior,
        message: 'Загружаем профиль…'
      },
    },
    ui: {
      'pageTitle': '#pageTitle'
    },
    initialize: function (options) {
      this.constructor.__super__.initialize.apply(this, arguments);

      //this.listenTo(App.renderChannel.vent, 'page:' + this.pageFile + ':ready', this.hideProgress);

      this.vent.on('page:ready', function (OctoberPageObj) {
        App.routerChannel.commands.execute('setTitle', $(OctoberPageObj.ui.pageTitle).text());
        OctoberPageObj.hideProgress();
      });
    },
    hideProgress: function () {
      this.triggerMethod('HideProgress');
      /*
      var context = this;
      setTimeout(function () {
        context.triggerMethod('HideProgress');
      }, 500);
      */
    }
  });
});