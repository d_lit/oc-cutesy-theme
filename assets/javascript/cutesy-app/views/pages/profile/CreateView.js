define(['app', 'behaviors/FormBehavior'],
  function (App, FormBehavior) { 'use strict';

  return App.Views.OctoberPage.extend({
    className   : 'cutesy-profile create',
    pageFile    : 'profile/create',
    behaviors: {
      FormBehavior: {
        behaviorClass   : FormBehavior,
        channelName     : 'profileChannel',
        ajaxHandler     : 'onCreate',
      },
    },
    ui: {
      'createBeautyForm'  : '#createBeautyForm',
      'createProForm'     : '#createProForm',
    },
    events: {

    },
    initialize: function(options) {
      this.constructor.__super__.initialize.apply(this, arguments);

      this.vent.on('page:ready', function (OctoberPageObj) {
        $(OctoberPageObj.ui.createBeautyForm).validate({
          submitHandler: function(form) {
            OctoberPageObj.triggerMethod('FormSubmit', form);
          }
        });

        $(OctoberPageObj.ui.createProForm).validate({
          submitHandler: function(form) {
            OctoberPageObj.triggerMethod('FormSubmit', form);
          }
        });
      });
    },
  });
});