define(['app', 'marionette'],
  function (App, Marionette) { 'use strict';

  return App.Views.OctoberPage.extend({
    className   : 'cutesy-intro',
    pageFile    : 'intro/default',
    initialize: function(options) {
      this.constructor.__super__.initialize.apply(this, arguments);

      this.vent.on('page:ready', function (OctoberPageObj) {
        OctoberPageObj.addRegions({ panelRegion  : '.community-search-panel'});

        require(['views/partials/intro/SearchPanelView'], function (PanelView) {
          OctoberPageObj.panelRegion.show(new PanelView());
        });
      });
    },
  });
});