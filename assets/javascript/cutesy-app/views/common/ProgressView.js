define(['app'],
  function (App) { 'use strict';

  var ProgressView = Marionette.ItemView.extend({
    className: 'progress',
    template : function (serialized_model) {
      var template_html = '<div class="message"><h3><i class="uk-icon-justify <%= iconClass %>"></i>&nbsp;<%= message %></h3></div>';

      var iconClass = serialized_model.iconClass;
      var message = serialized_model.message;

      return _.template(template_html) ({
          iconClass : iconClass,
          message : message
      });
    },
    serializeData: function(){
      return {
        iconClass: Marionette.getOption(this, 'iconClass'),
        message: Marionette.getOption(this, 'message')
      };
    },
  });

  return ProgressView;
});