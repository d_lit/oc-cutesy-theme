define(['app', 'behaviors/ProgressBehavior'],
  function (App, ProgressBehavior) { 'use strict';

  return App.module('Views', function (Views) {
    Views.OctoberPage = Marionette.LayoutView.extend({
      template: false,
      behaviors   : {
        ProgressBehavior: {
          behaviorClass: ProgressBehavior,
        },
      },
      initialize: function (options) {
        _.extend(this, _.pick(options, 'pageFile', 'parameters'));
        _.extend(this, {
          vent: new Backbone.Wreqr.EventAggregator(),
        });
        UIkit.domObserve(this.$el);
      },
      render: function () {
        var context = this;

        if (this.options.loadable === true) {
          context.triggerMethod('SetProgress');
        }

        App.renderChannel.reqres.request('getPage', {
          pageFile: this.pageFile,
          parameters: this.parameters
        }).done(function (response) {

          if (context.options.scrollable === true) {
            context.$el.on('click', 'a[data-page-scroll]', function (e) {
              e.preventDefault();
              context.$el.animate({
                scrollTop: $($.attr(this, 'href')).position().top + $(context.el).scrollTop()
              }, 800, 'easeInOutQuad');
            });
          }

          context.setTitle(response.pageTitle);
          context.setContent(response.pageContent);

          context.vent.trigger('page:ready', context);
          App.renderChannel.vent.trigger('page:' + context.pageFile + ':ready', context);
        });

        return context;
      },
      setTitle: function (title) {
        App.routerChannel.commands.execute('setTitle', title);
      },
      setContent: function (content) {
        if (this.options.loadable === true) {
          this.$el.append(content).fadeIn();
          return;
        }

        this.$el.hide().html(content).fadeIn();
      },
      onDestroy: function () {
        App.renderChannel.vent.off('page:' + this.pageFile + ':ready');
        this.vent.off('page:ready');
      }
    });
  });
});