define(['app', 'behaviors/ProgressBehavior'],
  function (App, ProgressBehavior) { 'use strict';

  return App.module('Views', function (Views) {
    Views.OctoberPartial = Marionette.ItemView.extend({
      template: false,
      behaviors   : {
        ProgressBehavior: {
          behaviorClass: ProgressBehavior,
        },
      },
      initialize: function (options) {
        _.extend(this, _.pick(options, 'component', 'partialFile', 'parameters'));
        _.extend(this, {
          vent: new Backbone.Wreqr.EventAggregator(),
        });
        UIkit.domObserve(this.$el);
      },
      render: function () {
        var context = this;

        if (this.options.loadable === true) {
          context.triggerMethod('SetProgress');
        }

        App.renderChannel.reqres.request('getPartial', {
          component   : this.component,
          partialFile : this.partialFile,
          parameters  : this.parameters
        }).done(function (response) {
          context.setContent(response.partialContent);

          context.vent.trigger('partial:ready', context);
          App.renderChannel.vent.trigger('partial:' + context.partialFile + ':ready', context);
        });

        return context;
      },
      setContent: function (content) {
        if (this.options.loadable === true) {
          this.$el.append(content).fadeIn();
          return;
        }

        this.$el.hide().html(content).fadeIn();
      },
      onDestroy: function () {
        App.renderChannel.vent.off('partial:' + this.partialFile + ':ready');
        this.vent.off('partial:ready');
      }
    });
  });
});