define(['app'],
    function (App) { 'use strict';

    return _.extend(Marionette.Region.prototype, {        
        attachHtml: function (view) {
            this.$el.hide();
            this.$el.html(view.el);
            this.$el.fadeIn();
        },
    });
});