define(['app'],
  function (App) { 'use strict';

  var ProgressBehavior = Marionette.Behavior.extend({
    ui: {
      'progress': '.progress'
    },
    defaults: {
      'message': 'Стартуем…',
      'iconClass': 'uk-icon-coffee'
    },
    onSetProgress: function () {
      var context = this;
      require(['views/common/ProgressView'], function (ProgressView) {
        var progressView = new ProgressView({
          iconClass: context.options.iconClass,
          message: context.options.message
         });

        context.view.$el.prepend(progressView.render().$el);
      });
    },
    onShowProgress: function () {
      this.view.$el.find('.progress').show();
    },
    onHideProgress: function () {
      var context = this;
      setTimeout(function () { context.view.$el.find('.progress').fadeOut(250); }, 500);
    }
  });

  return ProgressBehavior;
});