define(['app'],
  function (App) { 'use strict';

  return Marionette.Behavior.extend({
    /*
    initialize: function (options) {
      Backbone.Syphon.InputReaders.register('checkbox', function($el) {
        return $el.prop('checked') ? $el.prop('id') : null;
      });

      Backbone.Syphon.InputReaders.register('text', function($el) {
        return $el.val() ? $el.val() : null;
      });
    },
    */
    onFormSubmit: function (form) {
      var context = this,
        moduleName = this.options.moduleName,
        submitButtonElement = $("button[type='submit']", form),
        submitButtonText = submitButtonElement.html();

      var data = $(form).serializeArray();

      console.log(data);

      submitButtonElement
        .attr('disabled', true)
        .html('<i class="uk-icon-justify uk-icon-small uk-icon-coffee"></i>');

      App[this.options.channelName].reqres.request(this.options.ajaxHandler, data)
        .done(function (data, jqXHR) {
          submitButtonElement
            .attr('disabled', false)
            .html(submitButtonText);

          if (jqXHR.responseJSON.message) {
            UIkit.notify(jqXHR.responseJSON.message, { pos: 'top-center', status: 'success' });
          }
        })
        .fail(function (jqXHR) {
          submitButtonElement
            .attr('disabled', false)
            .html('<i class="uk-icon-justify uk-icon-small uk-icon-warning"></i>');

          UIkit.notify(jqXHR.responseText, { pos: 'top-center', status: 'danger' });
          return false;
        });
    },
  });
});