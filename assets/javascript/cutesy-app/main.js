require.config({
  baseUrl: 'themes/cutesy-theme/assets/javascript/cutesy-app',
  paths: {
    'backbone'          : '../../vendor/backbone/backbone-min',
    'backbone.syphon'   : '../../vendor/backbone.syphon/lib/backbone.syphon.min',
    'jquery'            : '../../vendor/jquery/dist/jquery.min',
    'jquery.deparam'    : '../../vendor/jquery-deparam/jquery-deparam',
    'jquery.ui'         : '../../vendor/jquery-ui/ui/',
    'jquery.validate'   : '../../vendor/jquery-validation/dist/jquery.validate.min',
    'history'           : '../../vendor/html5-history-api/history.min',
    'marionette'        : '../../vendor/marionette/lib/backbone.marionette.min',
    'uikit'             : '../../vendor/uikit/js/uikit.min',
    'underscore'        : '../../vendor/underscore/underscore-min',
    'framework'         : '../../../../../modules/system/assets/js/framework',
    'uploader'          : '../../../../../plugins/responsiv/uploader/assets/js/uploader',
    'dropzone'          : '../../../../../plugins/responsiv/uploader/assets/vendor/dropzone/dropzone',
  },
  shim: {
    'backbone' : {
      'deps' : [
        'jquery', 'underscore'
      ],
    },
    'backbone.syphon' : {
      'deps' : [
        'backbone'
      ],
    },
    'jquery.deparam' : {
      'deps' : [
        'jquery'
      ]
    },
    'jquery.ui' : {
      'deps' : [
        'jquery'
      ]
    },
    'jquery.validate' : {
      'deps' : [
        'jquery'
      ]
    },
    'marionette' : {
      'deps' : [
        'jquery', 'backbone'
      ],
    },
    'framework' : {
      'deps' : [
        'jquery'
      ]
    },
    'uikit' : {
      'deps' : [
        'jquery'
      ]
    },
    'dropzone' : {
      'deps' : [
        'jquery'
      ]
    },
    'uploader' : {
      'deps' : [
        'jquery', 'dropzone'
      ]
    },
  },
  config: {
    'uikit' : {
      'base' : '../../vendor/uikit/js/'
    }
  },
});

define([
  'app', 'classes/OctoberPage', 'classes/OctoberPartial', 'extends/RegionExtend',
  'modules/RouterModule', 'modules/RenderModule',  'modules/IntroModule',
  'modules/AccountModule', 'modules/ProfileModule', 'modules/CommunityModule',
], function (App) {
  App.start();
});