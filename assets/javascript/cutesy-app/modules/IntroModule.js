define(['app'],
  function (App) { 'use strict';

  return App.module('Intro', function (Intro) {
    App.on('before:start', function () {
      new Intro.Router({ controller: Intro.API });
    });

    Intro.Router = Marionette.AppRouter.extend({
      appRoutes: {
        '': 'showIntro',
      }
    });

    Intro.API = {
      showIntro: function () {
        require(['views/pages/intro/DefaultView'], function (DefaultView) {
          App.Views.Layout.contentRegion.show(new DefaultView());
        });
      },
    };
  });
});