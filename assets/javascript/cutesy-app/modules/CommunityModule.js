define(['app'],
  function (App) { 'use strict';

    var CommunityModule = App.module('Community', function (Community) {
      App.on('before:start', function () {
        new Community.Router({ controller: Community.API });
      });

      Community.Router = Marionette.AppRouter.extend({
        appRoutes: {
          '!community(/)(search?:query)': 'searchProfiles',
        }
      });

      Community.API = {
        searchProfiles: function (query) {
          var terms = query ? $.deparam(query) : {};
          require(['views/pages/community/SearchView'], function(SearchView) {
            App.Views.Layout.contentRegion.show(new SearchView({ loadable: true, terms: terms }));
          });
        },
      };

      App.accountChannel.vent.on('signin:done', function (form) {
        Community.API.searchProfiles();
        App.routerChannel.commands.execute('navigate', '!community');
      });

      App.communityChannel.vent.on('search:start', function (form) {

        var query = $(form).find('input').filter(function () { return !!this.value; }).serialize();

        if (query) {
          App.routerChannel.commands.execute('navigate', '!community/search?' + query);
        }
        else {
          App.routerChannel.commands.execute('navigate', '!community');
        }
      });

      App.communityChannel.reqres.setHandler('onSearch', function (options) {
        var defer = $.Deferred();

        $.request('communityComponent::onSearch', {
          data: options,
          success: function (data, statusText, jqXHR) {
            defer.resolve(data, jqXHR);
            App.communityChannel.vent.trigger('search:request:success', data);
          },
          error: function (jqXHR) {
            defer.reject(jqXHR);
            App.communityChannel.vent.trigger('search:request:error', data);
          }
        });

        var promise = defer.promise();
        return promise;
      });
    });

    return CommunityModule;
});