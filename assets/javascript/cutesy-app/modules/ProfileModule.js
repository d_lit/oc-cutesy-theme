define(['app'],
  function (App) { 'use strict';

  return App.module('Profile', function (Profile) {
    App.on('before:start', function () {
      new Profile.Router({ controller: Profile.API });
    });

    Profile.Router = Marionette.AppRouter.extend({
      appRoutes: {
        '!profile/create(/)(:role)'  : 'createProfile',
        '!profile/complete(/)': 'completeProfile',
        '!profile/update(/)'  : 'updateProfile',
        '!community/:alias': 'showProfile',
      }
    });

    Profile.API = {
      createProfile: function (role) {
        require(['views/pages/profile/CreateView'], function (CreateView) {
          App.Views.Layout.contentRegion.show(new CreateView({ parameters: { role: role } }));
        });
      },
      completeProfile: function () {
        require(['views/pages/profile/CompleteView'], function (CompleteView) {
          App.Views.Layout.contentRegion.show(new CompleteView());
        });
      },
      updateProfile: function () {
        require(['views/pages/profile/UpdateView'], function (UpdateView) {
          App.Views.Layout.contentRegion.show(new UpdateView());
        });
      },
      showProfile: function (alias) {
        require(['views/pages/profile/ShowView'], function (ShowView) {
          App.Views.Layout.contentRegion.show(new ShowView({ loadable: true, parameters: { alias: alias } }));
        });
      },
    };

    App.accountChannel.vent.on('register:done', function (options) {
      Profile.API.completeProfile();
      App.routerChannel.commands.execute('navigate', '!profile/complete');
    });

    App.profileChannel.vent.on('create:done', function (options) {
      Profile.API.completeProfile();
      App.routerChannel.commands.execute('navigate', '!profile/complete');
    });

    App.profileChannel.vent.on('complete:done update:done', function (options) {
      Profile.API.updateProfile();
      App.routerChannel.commands.execute('navigate', '!profile/update');
    });

    App.profileChannel.reqres.setHandler('onCreate', function (options) {
      var defer = $.Deferred();

      $.request('profileComponent::onCreate', {
        data: options,
        success: function (data, statusText, jqXHR) {
          defer.resolve(data, jqXHR);
          App.profileChannel.vent.trigger('create:done', data);
        },
        error: function (jqXHR) {
          defer.reject(jqXHR);
        }
      });

      var promise = defer.promise();
      return promise;
    });

    App.profileChannel.reqres.setHandler('onComplete', function (options) {
      var defer = $.Deferred();

      $.request('profileComponent::onComplete', {
        data: options,
        success: function (data, statusText, jqXHR) {
          defer.resolve(data, jqXHR);
          App.profileChannel.vent.trigger('complete:done', data);
        },
        error: function (jqXHR) {
          defer.reject(jqXHR);
        }
      });

      var promise = defer.promise();
      return promise;
    });

    App.profileChannel.reqres.setHandler('onUpdate', function (options) {
      var defer = $.Deferred();

      $.request('profileComponent::onUpdate', {
        data: options,
        success: function (data, statusText, jqXHR) {
          defer.resolve(data, jqXHR);
          App.profileChannel.vent.trigger('update:done', data);
        },
        error: function (jqXHR) {
          defer.reject(jqXHR);
        }
      });

      var promise = defer.promise();
      return promise;
    });
  });
});