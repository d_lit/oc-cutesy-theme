define(['app'],
  function (App) { 'use strict';

  return App.module('Account', function (Account) {
    App.on('before:start', function () {
      new Account.Router({ controller: Account.API });
    });

    Account.Router = Marionette.AppRouter.extend({
      appRoutes: {
        '!join(/)(:role)': 'createAccount',
      }
    });

    Account.API = {
      createAccount: function (role) {
        require(['views/pages/account/RegisterView'], function (RegisterView) {
            Account.API.closeSigninModal();
            App.Views.Layout.contentRegion.show(new RegisterView({ parameters: { role: role } }));
          }
        );
      },
      renderUserMenu: function (HeaderView) {
        require(['views/partials/header/UserMenuView'], function (UserMenuView) {
          $(HeaderView.ui.userMenu).html((new UserMenuView()).render().el);
        });
      },
      closeSigninModal: function () {
        if (Account.signinModal && Account.signinModal.isActive())
            Account.signinModal.hide();
      }
    };

    App.accountChannel.reqres.setHandler('onRegister', function (options) {
      var defer = $.Deferred();

      $.request('accountComponent::onRegister', {
        data: options,
        success: function (data, statusText, jqXHR) {
          defer.resolve(data, jqXHR);
          App.accountChannel.vent.trigger('register:done', options);
        },
        error: function (jqXHR) {
          defer.reject(jqXHR);
        }
      });

      var promise = defer.promise();
      return promise;
    });

    App.accountChannel.reqres.setHandler('onSignin', function (options) {
      var defer = $.Deferred();

      $.request('accountComponent::onSignin', {
        data: options,
        success: function (data, statusText, jqXHR) {
          defer.resolve(data, jqXHR);
          App.accountChannel.vent.trigger('signin:done', options);
        },
        error: function (jqXHR) {
          defer.reject(jqXHR);
        }
      });

      var promise = defer.promise();
      return promise;
    });

    App.renderChannel.vent.on('layout:default:ready', function (HeaderView) {
      require(['views/partials/account/SigninModalView'], function (SigninModalView) {
        App.Views.Layout.signinRegion.$el.append((new SigninModalView()).render().el);
        Account.signinModal = UIkit.modal(App.Views.Layout.signinRegion.el);
      });
    });

    App.renderChannel.vent.on('partial:header/default:ready', function (HeaderView) {

      Account.API.renderUserMenu(HeaderView);

      App.accountChannel.vent.on('register:done signin:done', function (options) {
        Account.API.renderUserMenu(HeaderView);
        Account.API.closeSigninModal();
      });

      App.profileChannel.vent.on('complete:done update:done', function (options) {
        Account.API.renderUserMenu(HeaderView);
      });
    });
  });
});