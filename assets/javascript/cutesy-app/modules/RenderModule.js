define(['app'],
  function (App) { 'use strict';

  var RenderModule = App.module('Render', function (Render) {
    App.renderChannel.reqres.setHandler('loadDefaultLayout', function (options) {
      var defer = $.Deferred();
      require(['views/layouts/DefaultView'], function (DefaultLayout) {
        defer.resolve(DefaultLayout);
        App.renderChannel.vent.trigger('layout:default:ready', DefaultLayout);
      });
      var promise = defer.promise();
      return promise;
    });

    App.renderChannel.reqres.setHandler('getPage', function (options) {
      var defer = $.Deferred();
      $.request('renderComponent::onRenderPage', {
        data: {
          pageFile  : options.pageFile,
          parameters: options.parameters
        },
        success: function(response) {
          defer.resolve(response);
        },
      });
      var promise = defer.promise();
      return promise;
    });

    App.renderChannel.reqres.setHandler('getPartial', function (options) {
      var defer = $.Deferred();
      $.request('renderComponent::onRenderPartial', {
        data: {
          component   : options.component,
          partialFile : options.partialFile,
          parameters  : options.parameters
        },
        success: function(response) {
          defer.resolve(response);
        },
      });
      var promise = defer.promise();
      return promise;
    });
  });

  return RenderModule;
});