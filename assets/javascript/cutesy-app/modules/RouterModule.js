define(['app'],
  function (App) { 'use strict';

  return App.module('Router', function (Router) {
    _.extend(Router, Marionette.AppRouter.extend());

    App.routerChannel.commands.setHandler('navigate', function (route) {
      Router.API.navigate(route);
    });

    App.routerChannel.commands.setHandler('setTitle', function (title) {
      Router.API.setTitle(title);
    });

    Router.API = {
      navigate: function (route,  options) {
        options = options || (options = {});
        Backbone.history.navigate(route, options);
      },
      setTitle: function (title) {
        $(document).prop('title', title + ' | Cutesy');
      }
    };
  });
});