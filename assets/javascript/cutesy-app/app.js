define([
  'underscore', 'jquery', 'backbone', 'marionette',
  'framework', 'uikit!grid,slider,sticky,notify',
  'jquery.deparam', 'jquery.ui/effect', 'jquery.validate',
  'dropzone', 'uploader'
], function () { 'use strict';

  var App = new Marionette.Application({
    initialize : function () {
      this.appName = 'Cutesy';
      this.globalChannel = Backbone.Wreqr.radio.channel('global');
      this.routerChannel = Backbone.Wreqr.radio.channel('router');
      this.renderChannel = Backbone.Wreqr.radio.channel('render');
      this.accountChannel = Backbone.Wreqr.radio.channel('account');
      this.profileChannel = Backbone.Wreqr.radio.channel('profile');
      this.communityChannel = Backbone.Wreqr.radio.channel('community');
    },
  });

  App.on('start', function () {
    App.renderChannel.reqres.request('loadDefaultLayout').done(function (DefaultLayout) {

        App.Views.Layout = new DefaultLayout();
        if (Backbone.history) {
          Backbone.history.start();
        }

    });
  });

  return App;
});